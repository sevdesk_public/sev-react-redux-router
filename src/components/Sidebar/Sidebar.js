import * as React from "react";
import SidebarLink from "../SidebarLink/SidebarLink";
import './Sidebar.css';

export default class Sidebar extends React.Component {
    state = {};

    render() {
        return (
            <aside className='Sidebar'>
                <div className={'logo'}>
                    <img src={'/sev-react-redux-router/logo.svg'}
                         alt={'Logo sevDesk'}/>
                    <h1>sev-react-redux-router</h1>
                </div>
                <ul>
                    <SidebarLink href={'#/'} title={'Home'}/>
                    <SidebarLink href={'#/contributors'} title={'Contributors'}/>
                    <SidebarLink href={'#/supporter'} title={'Supporter'}/>
                    <SidebarLink href={'#/license'} title={'License'}/>
                </ul>
                <div className='space'/>
                <div className='makeWithLove'>
                    <span>Made with</span>
                    <svg fill="#F44336" height="20" viewBox="0 0 24 24" width="20" xmlns="http://www.w3.org/2000/svg"
                         aria-label="love">
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path
                            d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"/>
                    </svg>
                    <span>in Offenburg</span>
                </div>
            </aside>
        );
    }
}