import * as React from "react";
import './NestedPageHeader.css';

export default class NestedPageHeader extends React.Component {
    state = {};

    render() {
        const {title} = this.props;
        return title ? (
            <header className={'NestedPageHeader'}>
                <h2>{title}</h2>
            </header>
        ) : null;
    }
}