import * as React from "react";
import {isRouteActive} from "../../router";
import './ListView.css';

export default class ListView extends React.Component {
    state = {};

    render() {
        const {items = []} = this.props;
        return (
            <ul className='ListView'>
                {items.map(item => (
                    <li key={item.href}>
                        <a title={item.title}
                           href={item.href}
                           className={(isRouteActive(item.href) ? 'active' : '')}>
                            <img src={item.image} alt={item.name}/>
                            <h4>{item.title}</h4>
                        </a>
                    </li>
                ))}
            </ul>
        );
    }
}