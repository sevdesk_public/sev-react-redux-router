import * as React from "react";
import './Loader.css';

export default class Loader extends React.Component {
    state = {};

    render() {
        return (
            <div className='Loader'>
                <div className="lds-dual-ring"/>
                <br/>
                <span>Loading...</span>
            </div>
        );
    }
}