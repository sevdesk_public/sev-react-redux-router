import * as React from "react";
import './NestedPage.css';
import NestedPageHeader from "../NestedPageHeader/NestedPageHeader";

export default class NestedPage extends React.Component {
    state = {};

    render() {
        const {children, title, className = ''} = this.props;
        return (
            <section className={'NestedPage ' + className}>
                <NestedPageHeader title={title}/>
                <main>
                    {children}
                </main>
            </section>
        );
    }
}