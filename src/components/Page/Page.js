import * as React from "react";
import './Page.css';

export default class Page extends React.Component {
    state = {};

    render() {
        const {children, className = ''} = this.props;
        return (
            <div className={'Page ' + className}>
                <main>
                    {children}
                </main>
            </div>
        );
    }
}