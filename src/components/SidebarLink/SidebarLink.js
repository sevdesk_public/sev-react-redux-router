import * as React from "react";
import {connect} from "react-redux";
import {isRouteActive} from "../../router";
import './SidebarLink.css';

class SidebarLink extends React.Component {
    state = {};

    render() {
        const {href = '#/', title = ''} = this.props;
        return (
            <li className={'SidebarLink'}>
                <a className={isRouteActive(href) ? 'active' : ''} href={href}>{title}</a>
            </li>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(SidebarLink);