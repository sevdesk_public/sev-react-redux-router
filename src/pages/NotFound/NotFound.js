import * as React from "react";
import Page from "../../components/Page/Page";
import NestedPageHeader from "../../components/NestedPageHeader/NestedPageHeader";
import "./NotFound.css";

export default class NotFound extends React.Component {
    render() {
        return (
            <Page className='NotFound'>
                <NestedPageHeader title={'Not found'}/>
                <div>
                    <h3>404</h3>
                </div>
            </Page>
        );
    }
}