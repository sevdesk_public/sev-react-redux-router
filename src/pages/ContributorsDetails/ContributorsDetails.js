import * as React from "react";
import NestedPage from "../../components/NestedPage/NestedPage";
import ContributorsService from "../../services/ContributorsService";
import Loader from "../../components/Loader/Loader";
import './ContributorsDetails.css';
import {connect} from "react-redux";

class ContributorsDetails extends React.Component {
    _isMounted = false;
    state = {
        loading: true,
        error: false,
        contributor: void 0
    };

    async componentDidMount() {
        const {router}= this.props;
        this._isMounted = true;
        try {
            this.setState({loading: true, error: false});
            const contributor = await ContributorsService.getContributor(router.current.params.contributorsId);
            this._isMounted && this.setState({contributor, loading: false});
        } catch (e) {
            this._isMounted && this.setState({contributor: void 0, loading: false, error: true});
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {contributor, loading, error} = this.state;
        return (
            <NestedPage className={'ContributorsDetails'} title={'Details'}>
                {contributor && <>
                    <img src={contributor.image} alt={contributor.name}/>
                    <p>
                        <b>Name:</b>
                        <br/>
                        <span>{contributor.name}</span>
                    </p>
                    <p>
                        <b>Job:</b>
                        <br/>
                        <span>{contributor.job}</span>
                    </p>
                </>}
                {error && <>
                    <h2>
                        Error<br/>
                        <small>Ups! Something went wrong!</small>
                    </h2>
                </>}
                {loading && <Loader/>}
            </NestedPage>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(ContributorsDetails);