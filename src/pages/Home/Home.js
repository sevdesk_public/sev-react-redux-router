import * as React from "react";
import Page from "../../components/Page/Page";
import NestedPageHeader from "../../components/NestedPageHeader/NestedPageHeader";
import SyntaxHighlighter from 'react-syntax-highlighter';
import {darcula} from 'react-syntax-highlighter/dist/esm/styles/hljs';
import './Home.css';

export default class Home extends React.Component {
    render() {
        return (
            <Page className='Home'>
                <NestedPageHeader title={'Home'}/>
                <main>
                    <p>
                        <code>sev-react-redux-router</code> is a router for react and redux. It is easy to use. it can
                        switch views in both parallel and interdependent.
                        We have programmed a router that is precisely adapted to our application.
                        This router however is not intended for seo sites.
                    </p>
                    <br/>
                    <h2>Getting started</h2>
                    <h4>Installation</h4>
                    <p>The best way to consume <code>sev-react-redux-router</code> is via the npm package which you can
                        install with npm (or
                        yarn if you prefer).</p>
                    <SyntaxHighlighter language="javascript" style={darcula}>
                        {`npm install sev-react-redux-router --save`}
                    </SyntaxHighlighter>
                    <p>To use <code>sev-react-redux-router</code>, you need the following packages too:</p>
                    <SyntaxHighlighter language="javascript" style={darcula}>
                        {`npm install history redux react-redux qs --save`}
                    </SyntaxHighlighter>
                    <br/>

                    <h2>How to use it</h2>
                    <h4>Easy set up and routing</h4>
                    <p>
                        The following implementation is only an example. It is no problem in case you have built redux
                        differently, you then just have to add the router reducer to the store.
                    </p>
                    <p>The examples are taken from this page. If you need more information, check the <a
                        target="_blank" rel="noopener noreferrer"
                        href={'https://gitlab.com/sevdesk_public/sev-react-redux-router'}>repository</a>.
                    </p>
                    <div className={'codePage'}>
                        <b><code>index.js</code></b>
                        <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                            {`import React from "react";
import ReactDOM from "react-dom";
import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux";
import {combineReducers} from "redux";
import {routerReducer, Router, Route} from "sev-react-redux-router";
import Home from "./pages/Home/Home";
import Contributors from "./pages/Contributors/Contributors";
import NotFound from "./pages/NotFound/NotFound";

const reducers = combineReducers({
    //other reducers
    router: routerReducer,
});
const store = createStore(
    reducers,
    //middleware
);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Route path="/" exact component={Home}/>
            <Route path="/contributors" component={Contributors}/>
            <Route path="/*" component={NotFound}/>
        </Router>
    </Provider>,
    document.getElementById('root'),
)`}
                        </SyntaxHighlighter>
                    </div>


                    <br/>
                    <h4>Parallel and nested routes</h4>
                    <p>Nested routes can easily be specified in other components. With the sev-react-redux-router,
                        however,
                        paths must always be specified as absolute values.</p>
                    <p>In the following example you can find out how to connect routes in parallel. Components can exist
                        simultaneously through an url.</p>
                    <div className={'codePage'}>
                        <b><code>./pages/Contributors/Contributors.js</code></b>
                        <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                            {`import * as React from "react";

export default class Contributors extends React.Component {
    render() {
        return (
            <div className='Contributors'>
                <Route path={'/contributors'} component={ContributorsList}/>
                <Route path={'/contributors/:contributorsId'} component={ContributorsDetails}/>
            </div>
        );
    }
}`}
                        </SyntaxHighlighter>
                    </div>

                    <br/>
                    <h4>How to get route data into the component?</h4>
                    <p>Please note that <code>this._isMounted</code> is only taken because we work with component state.
                        Normally redux is used and <code>this._isMounted</code> is not necessary.
                        If you want more information about <code>this._isMounted</code> you can find it <a
                            target="_blank" rel="noopener noreferrer"
                            href="https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html">here</a>.
                    </p>
                    <p>However, it is important to know that if a component depends on a route parameter, and the
                        parameter changes, the component will be remounted.</p>
                    <div className={'codePage'}>
                        <b><code>./pages/ContributorsDetails/ContributorsDetails.js</code></b>
                        <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                            {`import * as React from "react";
import ContributorsService from "../../services/ContributorsService";
import {connect} from "react-redux";

class ContributorsDetails extends React.Component {
    _isMounted = false;
    state = {
        contributor: {}
    };

    async componentDidMount() {
        const {router}= this.props;
        this._isMounted = true;
        try {
            const contributor = await ContributorsService.getContributor(
                router.current.params.contributorsId
            );
            this._isMounted && this.setState({contributor});
        } catch (e) {
            //Errorhandling ...
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {contributor = {}} = this.state;
        return (
            <div className="ContributorsDetails">
                <h2>{contributor.name}</h2>
                <p>{contributor.description}</p>
            </div>
        );
    }
}

const mapStateToProps = (state) => state;
export default connect(mapStateToProps)(ContributorsDetails);`}
                        </SyntaxHighlighter>
                    </div>
                    <br/>
                    <p>Here is a overview of the data we get from the store.
                        <br/>
                        <code>console.log(this.props.router)</code>
                    </p>
                    <SyntaxHighlighter language="json" style={darcula} showLineNumbers>
                        {`{
    prev: {
        pathname: "/contributors",
        search: "",
        searchObject: {},
        params: {},
        hash: ""
    },
    current: {
        pathname: "/contributors/0",
        search: "",
        searchObject: {},
        params: {
            contributorsId: "0"
        },
        hash: ""
    },
    routes: [
        {
            path: "/",
            id: "511cea53-...",
            exact: true
        }, {
            path: "/contributors",
            id: "55a989aa-..."
        }, {
            path: "/supporter",
            id: "16b5a72b-..."
        }, {
            path: "/license",
            id: "3d9dce34-..."
        }, {
            path: "/contributors",
            id: "0243cc0b-..."
        }, {
            path: "/contributors/:contributorsId",
            id: "209ec23c-..."
        }
    ]
}`}
                    </SyntaxHighlighter>
                    <br/>
                    <p>If you separate params with commas, you get an array</p>
                    <SyntaxHighlighter language="json" style={darcula} showLineNumbers>
                        {`{
    prev: {...},
    current: {
        pathname: "/contributors/0,1",
        search: "",
        searchObject: {},
        params: {
            contributorsId: ["0", "1"]
        },
        hash: ""
    },
    routes: [...]
}`}
                    </SyntaxHighlighter>
                    <br/>

                    <h4>Redirects</h4>
                    Redirects from one path to another.
                    <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                        {`<Redirect from="/somewhere" to="/contributors"/> //#/somewhere -> #/contributors
<Redirect from=">/somewhere/:id" to="/contributors/:id"/> //#/somewhere/0 -> #/contributors/0`}
                    </SyntaxHighlighter>
                    <p>At the moment any route with <code>*</code> is not allowed</p>

                    <h4>Navigation</h4>
                    <p>Navigating with a normal link looks like this:</p>
                    <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                        {`<a href="/contributors">Contributors</a>
<a href={'/contributors/' + value}>Contributors Details</a>`}
                    </SyntaxHighlighter>
                    <p>Programatically you can call the <code>push</code> function
                        via <code>sev-react-redux-router</code>.
                    </p>
                    <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                        {`import {push} from 'sev-react-redux-router';

push('/contributors');
push('/contributors/' + value);`}
                    </SyntaxHighlighter>

                    <br/>
                    <p>There is also a function of <code>sev-react-redux-router</code> that checks if the path is
                        correct.</p>
                    <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                        {`import {isRouteActive} from 'sev-react-redux-router';

//URL is #/contributors/0

console.log(isRouteActive("/contributors")); //true
console.log(isRouteActive("/contributors/0")); //true
console.log(isRouteActive("#/contributors/0")); //true

console.log(isRouteActive("/")); //false -> because in this example is linked with a exact route. otherwise would be true
console.log(isRouteActive("/contributors/1")); //false
console.log(isRouteActive("/license")); //false
`}
                    </SyntaxHighlighter>

                    <br/>
                    <h4>Lazy loading route</h4>
                    <p>In combination with React.lazy and Suspense lazy loading is simple.</p>
                    <SyntaxHighlighter language="javascript" style={darcula} showLineNumbers>
                        {`import React, {Suspense} from 'react';
import Loader from "./components/Loader/Loader";
...

const License = React.lazy(() => import('./pages/License/License'));

ReactDOM.render(
    <Provider store={store}>
    	<Suspense fallback={<Loader />}>
            <Router>
                ...
                <Route path="/license" component={License}/>
            </Router>
        </Suspense>
    </Provider>,
    document.getElementById('root'),
)`}
                    </SyntaxHighlighter>
                </main>
            </Page>
        );
    }
}