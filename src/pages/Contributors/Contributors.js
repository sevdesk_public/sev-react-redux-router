import * as React from "react";
import Page from "../../components/Page/Page";
import ContributorsList from "../ContributorsList/ContributorsList";
import ContributorsDetails from "../ContributorsDetails/ContributorsDetails";
import {Route} from "../../router";

export default class Contributors extends React.Component {
    render() {
        return (
            <Page className={'Contributors'}>
                <Route path={'/contributors'}
                       state={'contributors.list'}
                       component={ContributorsList}/>
                <Route path={'/contributors/:contributorsId'}
                       state={'contributors.details'}
                       component={ContributorsDetails}/>
            </Page>
        );
    }
}