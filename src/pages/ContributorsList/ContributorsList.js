import * as React from "react";
import NestedPage from "../../components/NestedPage/NestedPage";
import ListView from "../../components/ListView/ListView";
import ContributorsService from "../../services/ContributorsService";
import Loader from "../../components/Loader/Loader";
import './ContributorsList.css';

export default class ContributorsList extends React.Component {
    _isMounted = false;
    state = {
        loading: true,
        contributors: []
    };

    async componentDidMount() {
        this._isMounted = true;
        this.setState({loading: true});
        const contributors = await ContributorsService.getContributors();
        this._isMounted && this.setState({contributors, loading: false});
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {contributors, loading} = this.state;
        return (
            <NestedPage className={'ContributorsList'} title={'Contributors'}>
                <ListView items={contributors.map(contributor => ({
                    href: `#/contributors/${contributor.id}`,
                    image: contributor.image,
                    title: contributor.name,
                }))}/>
                {loading && <Loader/>}
            </NestedPage>
        );
    }
}