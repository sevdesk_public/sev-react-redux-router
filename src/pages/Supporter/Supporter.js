import * as React from "react";
import Page from "../../components/Page/Page";
import NestedPageHeader from "../../components/NestedPageHeader/NestedPageHeader";
import './Supporter.css';

export default class Supporter extends React.Component {
    render() {
        return (
            <Page className='Supporter'>
                <NestedPageHeader title={'Supporter'}/>
                <main>
                    <img width={300} alt={'Logo sevDesk'}
                         src={'https://sevdesk.de/content/themes/sevDesk/images/sevdesk-logo.svg'}/>

                    <br/>
                    <br/>
                    <p>
                        We are a fast growing software startup from Offenburg, at the foot of the Black Forest. With our
                        cloud-based accounting and financial management software, we take a lot of the burden off the
                        self-employed, freelancers and small businesses. Our software offers the ideal solution for a
                        careful, proper and digital accounting, with which you can easily get a grip on your finances.
                    </p>
                    <p>
                        More information about the company or product can be found <a href={'https://sevdesk.de/'}
                                                                           target={'_blank'}
                                                                           rel="noopener noreferrer">here</a>.
                    </p>
                    <br/>
                    <img src={'https://cdn.sevdesk.de/uploads/schlittenfahrt-sevdesk.jpg'} alt={'sevSledding'}/>
                    <p>
                        Interested in working as a developer at sevdesk. We also <a
                        href={'https://sevdesk.de/karriere/'} target={'_blank'} rel="noopener noreferrer">hire</a>.
                    </p>
                </main>
            </Page>
        );
    }
}