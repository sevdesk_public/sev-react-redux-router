import * as React from "react";
import {connect} from 'react-redux';
import {removeRoute, addRoute} from "./actions";
import * as Service from "./Service";

class Route extends React.Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            changedPath: void 0,
            update: false,
            id: Service.getRouteId()
        };
    }

    componentDidMount() {
        const {dispatch, path, router, onEnter, exact} = this.props;
        const {id} = this.state;
        this._isMounted = true;
        const isRouteActive = router.current.pathname.indexOf(path) !== -1;
        if (isRouteActive && onEnter) {
            onEnter(path);
        }
        dispatch(addRoute({path, id, exact}));
    }

    componentWillUnmount() {
        const {dispatch, path, onLeave} = this.props;
        const {id} = this.state;
        onLeave && onLeave(path);
        dispatch(removeRoute({path, id}));
    }

    componentDidUpdate(prevProps, prevState) {
        const {path, router, onEnter} = this.props;
        const {changedPath} = this.state;
        const isRouteActive = Service.isRouteActiveForRoute(path);
        const isRouteParamsChanged = Service.isRouteParamsChanged(path, router.current, router.prev);
        const nextChangedPath = Service.getNextChangedPath(path);
        if (isRouteActive && isRouteParamsChanged && changedPath !== nextChangedPath) {
            this._isMounted && this.setState({
                    changedPath: nextChangedPath || void 0,
                    update: true
                },
                () => {
                    if (this._isMounted) {
                        this.setState({update: false});
                        onEnter && onEnter(path);
                    }
                }
            );
        }
    }

    render() {
        const {update} = this.state;
        const {component, path, exact = false} = this.props;
        const isRouteActive = Service.isRouteActiveForRoute(path, exact);
        if (component && isRouteActive && !update) {
            return React.createElement(component, this.props);
        } else {
            return null;
        }
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(Route);