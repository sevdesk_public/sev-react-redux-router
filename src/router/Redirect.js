import * as React from "react";
import {connect} from 'react-redux';
import * as Service from "./Service";

class Redirect extends React.Component {

    componentDidUpdate(prevProps, prevState) {
        const {from, to} = this.props;
        const isRouteActive = Service.isRedirectActive(from);
        if (isRouteActive) {
            Service.push(Service.getPathWithReplacedParams(to));
        }
    }

    render() {
        return null;
    }
}

const mapStateToProps = (state) => {
    return state;
};
export default connect(mapStateToProps)(Redirect);