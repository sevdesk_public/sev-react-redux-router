import {ROUTER_CHANGE_HISTORY, ROUTER_ADD_ROUTE, ROUTER_REMOVE_ROUTE} from "./types";

export const changeHistory = ({prev, current, history}) => ({
    type: ROUTER_CHANGE_HISTORY,
    payload: {prev, current, history}
});

export const addRoute = (route) => ({
    type: ROUTER_ADD_ROUTE,
    payload: route
});

export const removeRoute = (id) => ({
    type: ROUTER_REMOVE_ROUTE,
    payload: id
});