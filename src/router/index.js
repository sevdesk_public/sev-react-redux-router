import * as historyCopy from './history';
import * as routerReducerCopy from './reducer';
import * as RouterCopy from './Router';
import * as RouteCopy from './Route';
import * as RedirectCopy from './Redirect';
import * as actionsCopy from './actions';
import * as typesCopy from './types';
import * as SerivceCopy from './Service';

export const routerReducer = routerReducerCopy.default;
export const Router = RouterCopy.default;
export const Route = RouteCopy.default;
export const Redirect = RedirectCopy.default;
export const actions = actionsCopy;
export const types = typesCopy;
export const history = historyCopy.default;
export const push = SerivceCopy.push;
export const isRouteActive = SerivceCopy.isRouteActive;