import {ROUTER_ADD_ROUTE, ROUTER_REMOVE_ROUTE, ROUTER_CHANGE_HISTORY} from "./types";
import * as Service from "./Service";
import history from "./history";

const initialState = {
    prev: void 0,
    current: void 0,
    history: [],
    routes: []
};
const routerReducer = (state = initialState, {type, payload}) => {
    let routes = [];
    let current;
    switch (type) {
        case ROUTER_CHANGE_HISTORY:
            return {...state, ...payload};
        case ROUTER_ADD_ROUTE:
            routes = [...(state.routes || []), payload];
            Service.setRoutes(routes);
            current = Service.getHistoryObject(history.location);
            return {
                ...state,
                current,
                routes
            };
        case ROUTER_REMOVE_ROUTE:
            routes = (state.routes || []).filter(route => route.id !== payload.id);
            Service.setRoutes(routes);
            current = Service.getHistoryObject(history.location);
            return {
                ...state,
                current,
                routes
            };
        default:
            return state;
    }
};

export default routerReducer;