import {parse} from "qs";
import history from './history';

let _routes = [];

export const getHistoryObject = (location = {}) => {
    const {pathname, search = '', hash} = location;
    const searchObject = parse(search.replace('?', '')) || {};
    const params = getParams(pathname);
    return {
        pathname,
        search,
        searchObject,
        params,
        hash
    };
};


export const getActiveRoutes = () => {
    const {location} = history;
    const activeRoutes = getAllActiveRoutes(location.pathname);
    const exactActiveRoute = activeRoutes.find(item => item.exact);
    if (exactActiveRoute) {
        return [exactActiveRoute];
    } else {
        return activeRoutes;
    }
};

export const getAllActiveRoutes = (pathname) => {
    return _routes.filter(item => {
        const routePathParts = item.path.split('/');
        const locationPathParts = pathname.split('/');
        return routePathParts.every((routePathPart, index) => {
            const locationPathPart = locationPathParts[index];
            const isStarPathPart = locationPathPart && routePathPart && routePathPart === '*';
            const isParamPathPart = locationPathPart && routePathPart && routePathPart.indexOf(':') !== -1;
            const isSamePath = routePathPart === locationPathPart;
            if (item.exact) {
                return isSamePath || isParamPathPart;
            } else {
                return isSamePath || isParamPathPart || isStarPathPart || !routePathPart;
            }
        });
    });
};

export const isRouteActive = (path) => {
    const {location} = history;
    const locationPathname = location.pathname;
    if (path.replace('#', '') === locationPathname) {
        return true;
    } else if (path) {
        const pathRoutes = getAllActiveRoutes(path);
        const currentRoutes = getAllActiveRoutes(locationPathname);
        const activeRoutes = pathRoutes.filter(pathRoute => {
            return currentRoutes.some(currentRoute => {
                const pathParts = path.split('/').length;
                const locationParts = locationPathname.split('/').length;
                const sameId = pathRoute.id === currentRoute.id;
                const hasNoStar = currentRoute.path.indexOf('*') === -1;
                return sameId && pathParts <= locationParts && hasNoStar;
            });
        });
        return activeRoutes.length > 0;
    }
    return false;
};

export const isRedirectActive = (from = '') => {
    const {location} = history;
    const pathname = location.pathname;
    if (from.replace('#', '') === pathname) {
        return true;
    } else if (from) {
        const fromParts = from.split('/');
        const pathnameParts = pathname.split('/');
        const sameLength = fromParts.length === pathnameParts.length;
        const sameParts = fromParts.every((formPart, index) => {
            if (formPart.indexOf(':') !== -1) {
                return pathnameParts[index] && pathnameParts[index].length > 0;
            } else if (formPart === pathnameParts[index]) {
                return true;
            } else {
                return formPart === '*' && pathnameParts[index];
            }
        });
        const hasPathStar = from.indexOf('*') !== -1;
        return (sameLength || hasPathStar) && sameParts;
    }
    return false;
};

export const isRouteActiveForRoute = (path) => {
    if (path) {
        const activeRoutes = getActiveRoutes();
        return activeRoutes.some(activeRoute => {
            if (activeRoute.path.indexOf('*') !== -1) {
                return activeRoute.path === path && activeRoutes.length === 1;
            } else {
                return activeRoute.path === path;
            }
        });
    }
    return false;
};

export const push = (data) => {
    return history.push(data);
};

export const isRouteParamsChanged = (path, current, prev) => {
    if (current && prev && path) {
        const routePathParts = path.split('/');
        const currentPathParts = current.pathname.split('/');
        const prevPathParts = prev ? prev.pathname.split('/') : [];
        if (prevPathParts.length > 0) {
            return routePathParts.some((routePathPart, index) => {
                const currentPathPart = currentPathParts[index];
                const prevPathPart = prevPathParts[index];
                const isRoutePathParam = routePathPart && routePathPart.indexOf(':') !== -1;
                return isRoutePathParam && prevPathPart && currentPathParts && currentPathPart !== prevPathPart;
            });
        }
    }
    return false;
};

export const getNextChangedPath = (path) => {
    if (history && history.location) {
        const pathParts = (path || '').split('/');
        const currentPathParts = history.location.pathname.split('/');
        if (pathParts.length <= currentPathParts.length) {
            currentPathParts.length = pathParts.length;
            return currentPathParts.join('/');
        } else {
            return history.location.pathname;
        }
    }
    return void 0;
};

export const getParams = () => {
    const {location} = history;
    const pathParts = location.pathname.split('/');
    const activeRoutes = getActiveRoutes();
    const params = {};
    activeRoutes.forEach((activeRoute) => {
        const activeRoutPathParts = activeRoute.path.split('/');
        const activeRouteParamKeysWithPosition = activeRoutPathParts
            .map((key, position) => key.indexOf(':') !== -1 ? {key, position} : void 0)
            .filter((item) => item && item.key);
        activeRouteParamKeysWithPosition.forEach(({key, position}) => {
            const value = pathParts[position];
            const arrayValue = value.split(',');
            if (arrayValue.length > 1) {
                params[key.replace(':', '')] = arrayValue;
            } else {
                params[key.replace(':', '')] = value;
            }
        });
    });
    return params;
};

export const getPathWithReplacedParams = (path = '') => {
    const {location} = history;
    const pathname = location.pathname;
    const pathParts = path.split('/');
    const pathnameParts = pathname.split('/');
    const replacedParts = pathParts.map((formPart, index) => {
        if (formPart.indexOf(':') !== -1 && pathnameParts[index] && pathnameParts[index].length > 0) {
            return pathnameParts[index];
        } else {
            return formPart;
        }
    });
    return replacedParts.join('/');
};

export const getRouteId = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (char) => {
        const random = Math.random() * 16 | 0;
        const value = char === 'x' ? random : (random & (0x3 | 0x8));
        return value.toString(16);
    });
};

export const getRoutes = () => {
    return _routes;
};

export const setRoutes = (routes) => {
    _routes = routes;
};
