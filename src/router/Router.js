import * as React from "react";
import {connect} from 'react-redux';
import history from './history';
import {changeHistory} from "./actions";
import * as Service from "./Service";

class Router extends React.Component {

    constructor(props) {
        super(props);
        this._unlisten = void 0;
        this.state = {
            ready: false
        };
        this.initHistory = this.initHistory.bind(this);
        this.onChangeHistory = this.onChangeHistory.bind(this);
    }


    componentDidMount() {
        window.addEventListener("hashchange", this.onChangeHistory, false);
        this._unlisten = history.listen(this.onChangeHistory);
        this.initHistory();
    }

    componentWillUnmount() {
        this._unlisten();
    }

    componentDidUpdate() {
        window.removeEventListener("hashchange", this.onChangeHistory, false);
    }

    initHistory() {
        const {router, dispatch} = this.props;
        const current = Service.getHistoryObject(history.location);
        dispatch(changeHistory({
            routes: router.routes,
            history,
            prev: router.current,
            current
        }));
        this.setState({ready: true});
    };

    onChangeHistory() {
        const {location} = history;
        const {router, dispatch} = this.props;
        const current = Service.getHistoryObject(location, router.routes);
        dispatch(changeHistory({
            history,
            routes: router.routes,
            prev: router.current,
            current
        }));
    };

    render() {
        const {children} = this.props;
        const {ready} = this.state;
        if (children && ready) {
            return children;
        } else {
            return null;
        }
    }
}

const mapStateToProps = (state) => {
    return state;
};
export default connect(mapStateToProps)(Router);