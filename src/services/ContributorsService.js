let contributors = [
    {
        id: '0',
        image: 'https://assets.gitlab-static.net/uploads/-/system/user/avatar/3221222/avatar.png?width=90',
        name: 'Aaron Pollvogt',
        job: 'Web- & Mobile Developer',
    }
];

export default class ContributorsService {

    static getContributor(id) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const contributor = contributors.find(item => item.id === id);
                if (contributor) {
                    resolve(contributor);
                } else {
                    reject();
                }
            }, 1000);
        });
    }

    static getContributors() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(contributors);
            }, 1000);
        });
    }
}