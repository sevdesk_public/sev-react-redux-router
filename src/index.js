import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import {createStore, applyMiddleware} from "redux";
import {createLogger} from "redux-logger";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {combineReducers} from "redux";
import {routerReducer, Router, Route, Redirect} from "./router"; //import {...} from 'sev-react-redux-router';
import Loader from "./components/Loader/Loader";
import Sidebar from "./components/Sidebar/Sidebar";
import Home from "./pages/Home/Home";
import Contributors from "./pages/Contributors/Contributors";
import Supporter from "./pages/Supporter/Supporter";
import NotFound from "./pages/NotFound/NotFound";
import "./index.css";

const License = React.lazy(() => import('./pages/License/License'));


const middleware = [thunk];
if (process.env.NODE_ENV !== "production") {
    middleware.push(createLogger());
}
const reducers = combineReducers({
    router: routerReducer
});
const store = createStore(
    reducers,
    applyMiddleware(...middleware)
);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <Suspense fallback={<Loader/>}>
                <Sidebar/>
                <Router>
                    <Route path="/" component={Home} exact/>
                    <Route path="/contributors" component={Contributors}/>
                    <Route path="/supporter" component={Supporter}/>
                    <Route path="/license" component={License}/>
                    <Route path="/*" component={NotFound}/>
                    <Redirect from="/somewhere" to="/contributors"/>
                    <Redirect from="/somewhere/:id" to="/contributors/:id"/>
                </Router>
            </Suspense>
        </Provider>
    </React.StrictMode>,
    document.getElementById("root")
);
