import * as historyCopy from './src/router/history';
import * as routerReducerCopy from './src/router/reducer';
import * as RouterCopy from './src/router/Router';
import * as RouteCopy from './src/router/Route';
import * as RedirectCopy from './src/router/Redirect';
import * as actionsCopy from './src/router/actions';
import * as typesCopy from './src/router/types';
import * as SerivceCopy from './src/router/Service';

export const routerReducer = routerReducerCopy.default;
export const Router = RouterCopy.default;
export const Route = RouteCopy.default;
export const Redirect = RedirectCopy.default;
export const actions = actionsCopy;
export const types = typesCopy;
export const history = historyCopy.default;
export const push = SerivceCopy.push;
export const isRouteActive = SerivceCopy.isRouteActive;
